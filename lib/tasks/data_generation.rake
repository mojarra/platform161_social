namespace :data do
  desc "create api key to access to the api"
  task :generate_api_key => :environment do
    api_key = ApiKey.create!
    puts "The access token to make queries to the API is: #{api_key.access_token}"
  end

  desc "create fake data in the DB"
  task :generate_random_test_data => :environment do
    20.times do
      username = Faker::Internet.user_name
      email    = Faker::Internet.free_email(username)
      User.create!(email: email,
                   username: username,
                   password: "platform161",
                   password_confirmation:"platform161")
    end

    User.all.each do |user|
      10.times do
        user.posts.create!(content: Faker::Lorem.sentence,
                           created_at: Faker::Time.between(10.days.ago, DateTime.now))
      end

      User.order("RANDOM()").limit(5).each do |user_to_follow|
        user.follow(user_to_follow)
      end
    end

    puts "Created the next 20 email(username) users:\n"
    puts User.all.map{ |user| "#{user.email}(#{user.username})" }.join("\n")
    puts "to access as any user use its eamil or username and the password 'platform161'\n"
  end
end
