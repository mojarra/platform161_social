object @user

attributes :id, :username, :email, :created_at

child :following do
  attributes :id, :username, :email, :created_at
end
