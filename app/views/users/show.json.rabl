object @user

attributes :id, :username, :email, :created_at

child :posts do
  attributes :content, :created_at
end
