object @user

attributes :id, :username, :email, :created_at

child :followers do
  attributes :id, :username, :email, :created_at
end
