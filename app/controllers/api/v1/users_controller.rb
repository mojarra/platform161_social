module Api
  module V1
    class UsersController < BaseController
      before_filter :set_user

      def show
        respond_with @user.posts
      end

      def followers
        respond_with @user.followers
      end

      def following
        respond_with @user.following
      end

      private

      def set_user
        @user = User.find(params[:id])
      end
    end
  end
end
