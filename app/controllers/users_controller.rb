class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_user
  before_filter :set_counters, except: %w[ follow ]

  layout false, only: %w[ follow ]

  def show
    @posts = @user.posts
  end

  def followers
    @followers = @user.followers
  end

  def following
    @following = @user.following
  end

  def follow
    if current_user.following?(@user)
      current_user.unfollow(@user)
    else
      current_user.follow(@user)
    end

    set_counters

    render nothing: true
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def set_counters
    @followers_count = @user.followers.count
    @following_count = @user.following.count
    @posts_count     = @user.posts.count
  end
end
