class PostsController < ApplicationController
  before_filter :authenticate_user!

  layout false, only: %w[ create ]

  def create
    @post = Post.create!(post_params)

    render partial: 'post', locals: { post: @post }
  end

  private

  def post_params
    params[:post].permit(:content).merge({ user_id: current_user.id })
  end
end
