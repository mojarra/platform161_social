class WelcomeController < ApplicationController
  before_filter :authenticate_user!, except: %w[ index ]
  before_filter :index_redirection, only: %W[ index ]

  def index
    @posts = current_user.feed
  end

  private

  def index_redirection
    unless user_signed_in?
      redirect_to new_user_session_path
    end
  end
end
