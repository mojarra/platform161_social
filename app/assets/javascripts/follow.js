// Edit button hookups

function bindFollowButtons(){
  if ($("#wall-header").length > 0){
    $('.follow-btn').on('click', function(evt){
      evt.preventDefault();
      var $user_id = $(this).data('user-id');
      var $url = Routes.follow_user_path({ id: $user_id })
      var that = this

      $.get($url, function(data){
        location.reload();
      }).fail(function(e){
        BootstrapDialog.alert("Error " + e.status + ": " +e.statusText);
      });
    });
  }
}

Platform161.bindFollowButtons = bindFollowButtons;
