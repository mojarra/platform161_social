// Edit button hookups

function bindNewPostForm(){
  $('.new-post-btn').on('click', function(evt){
     evt.preventDefault();
     var $data          = $("#new_post").serialize();
     var $url           = $("#new_post").attr('action');

     $.ajax({ method: 'POST',
              url: $url,
              data: $data
     }).done(function(data){
       $("#last-news").prepend(data)
       $('textarea#post_content', '#new_post').val("");
     }).fail(function(e){
        BootstrapDialog.alert("Error:" + e.status + ": " + e.statusText);
     });
  });

  $('textarea#post_content', '#new_post').on('keypress', function(e){
    if (this.value.length > 159 && $.inArray(e.keyCode,[8,13,46,9]) == -1){
      return false;
    }
  });
}

Platform161.bindNewPostForm = bindNewPostForm;
