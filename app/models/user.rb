class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :email,    presence: true, uniqueness: true
  validates :username, presence: true, uniqueness: true
  # Only allow letter, number, underscore and punctuation.
  validates_format_of :username, with: /^[a-zA-Z0-9_\.]*$/, :multiline => true,
                      message: "is invalid Only letter, number, underscore and punctuation."

  has_many :posts, dependent: :destroy
  has_many :following_relationships, class_name:  "FollowRelationship",
                                     foreign_key: "follower_id",
                                     dependent:   :destroy

  has_many :followers_relationships, class_name:  "FollowRelationship",
                                     foreign_key: "followed_id",
                                     dependent:   :destroy

  has_many :following, through: :following_relationships, source: :followed
  has_many :followers, through: :followers_relationships, source: :follower

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where(["username = :value OR email = :value", { :value => login }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      where(conditions.to_hash).first
    end
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username
  end

  def follow(other_user)
    following_relationships.create(followed_id: other_user.id)
  end

  def unfollow(other_user)
    following_relationships.find_by(followed_id: other_user.id).destroy
  end

  def following?(other_user)
    following.include?(other_user)
  end

  def feed
    Post.where("EXISTS(SELECT *
                       FROM follow_relationships
                       WHERE (posts.user_id = #{self.id}) OR
                             (follow_relationships.follower_id = #{self.id} AND
                              follow_relationships.followed_id = posts.user_id))")
  end
end
