class Post < ActiveRecord::Base
  validates :content, presence: true, length: { maximum: 160 }
  validates :user_id, presence: true

  belongs_to :user

  default_scope { order(created_at: :desc) }
end
