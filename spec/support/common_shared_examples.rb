require 'rails_helper'

shared_examples_for "field can't be nil" do
  before do
    subject.send(:write_attribute, field, nil)
    subject.valid?
  end

  it { expect(subject).to_not be_valid }
  it { expect(subject.errors[field]).to include("can't be blank") }
end

shared_examples_for "field can't be blank" do
  before do
    subject.send(:write_attribute, field, "")
    subject.valid?
  end

  it { expect(subject).to_not be_valid }
  it { expect(subject.errors[field]).to include("can't be blank") }
end

shared_examples_for "field should be uniq" do
  let(:dup)    { subject.dup }

  before { dup.valid? }

  it { expect(dup).to_not be_valid }
  it { expect(dup.errors[field]).to include("has already been taken") }
end

shared_examples_for "field should contains only '.', numbers and letters" do
  before do
    subject.send(:write_attribute, field, "asdfasd%&/srq")
    subject.valid?
  end

  it { expect(subject).to_not be_valid }
  it { expect(subject.errors[field]).to include("is invalid Only letter, number, underscore and punctuation.") }
end

shared_examples_for "field can't overcome max_length" do
  before do
    subject.send(:write_attribute, field, "a" * (max_length + 1))
    subject.valid?
  end

  it { expect(subject).to_not be_valid }
  it { expect(subject.errors[field]).to include("is too long (maximum is #{max_length} characters)") }
end
