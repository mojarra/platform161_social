FactoryGirl.define do
  factory :post, class: Post do
    content                "post content"
    user
  end
end
