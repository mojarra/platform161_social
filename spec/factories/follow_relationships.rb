FactoryGirl.define do
  factory :follow_relationship, class: FollowRelationship do
    association :followed, factory: :user
    association :follower, factory: :user
  end
end
