FactoryGirl.define do
  factory :user, class: User do
    sequence(:username)    { |n| "j.smith#{n}" }
    sequence(:email)       { |n| "j.smith#{n}@example.com" }
    password               "password"
    password_confirmation  "password"
  end
end
