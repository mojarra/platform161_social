require 'rails_helper'

RSpec.describe PostsController, :type => :controller do
  let(:user)                 { FactoryGirl.create(:user) }

  describe "POST create" do
    context "user logged" do
      before {
        sign_in user
        expect(Post).to receive(:create!)
      }

      it "render the show template" do
        post :create, { post: { content: "content" } }

        expect(response).to render_template("posts/_post")
      end
    end

    context "user not logged" do
      it "redirects to user loggin page" do
        post :create, { post: { content: "content" } }

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
