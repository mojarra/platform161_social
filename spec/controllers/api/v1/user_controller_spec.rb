require 'rails_helper'

RSpec.describe Api::V1::UsersController, :type => :controller do
  let(:api_key)              { FactoryGirl.create(:api_key) }
  let(:user)                 { FactoryGirl.create(:user) }
  let(:user_following)       { FactoryGirl.create(:user) }
  let!(:follow_relationship) { FactoryGirl.create(:follow_relationship,
                                                   follower: user,
                                                   followed: user_following) }
  let!(:user_post)           { FactoryGirl.create(:post, user: user) }

  describe "GET show" do
    context "with access_token" do
      before do
        set_access_token
        get :show, {id: user.id, format: :json}
      end

      it "render the show template" do
        expect(response.status).to eq 200
        expect(response_body).to include(JSON.parse(user_post.to_json))
      end
    end

    context "without access_token" do
      before do
        get :show, {id: user.id, format: :json}
      end

      it "render the show template" do
        expect(response.status).to eq 401
      end
    end
  end

  describe "GET following" do
    context "with access_token" do
      before do
        set_access_token
        get :following, { id: user.id, format: :json }
      end

      it "update the wall header" do
        expect(response.status).to eq 200
        expect(response_body).to include(JSON.parse(user_following.to_json))
      end
    end

    context "without access_token" do
      before do
        get :following, {id: user.id, format: :json}
      end

      it "render the show template" do
        expect(response.status).to eq 401
      end
    end
 end

  describe "GET followers" do
    context "with access_token" do
      before do
        set_access_token
        get :followers, { id: user_following.id, format: :json }
      end

      it "update the wall header" do
        expect(response.status).to eq 200
        expect(response_body).to include(JSON.parse(user.to_json))
      end
    end

    context "without access_token" do
      before do
        get :followers, {id: user_following.id, format: :json}
      end

      it "render the show template" do
        expect(response.status).to eq 401
      end
    end
  end

  def response_body
    JSON.parse(response.body)
  end

  def set_access_token
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(api_key.access_token)
  end
end
