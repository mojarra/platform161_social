require 'rails_helper'

RSpec.describe UsersController, :type => :controller do
  let(:user)                 { FactoryGirl.create(:user) }
  let(:user_following)       { FactoryGirl.create(:user) }
  let(:user_not_following)   { FactoryGirl.create(:user) }
  let!(:follow_relationship) { FactoryGirl.create(:follow_relationship,
                                                   follower: user,
                                                   followed: user_following) }

  describe "GET show" do
    context "user logged" do
      before { sign_in user }

      it "render the show template" do
        get :show, { id: user.id }

        expect(response).to render_template("show")
      end
    end

    context "user not logged" do
      it "redirects to user loggin page" do
        get :show, { id: user.id }

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe "GET follow" do
    context "user logged" do
      before do
       sign_in user
       expect(controller).to receive(:current_user).twice.and_return(user)
      end

      context "user followed" do
        it "update the wall header" do
          expect(user).to receive(:unfollow).with(user_following)
          get :follow, { id: user_following.id }
          expect(response).to render_template(nil)
        end
      end

      context "user not followed" do
        it "update the wall header" do
          expect(user).to receive(:unfollow).with(user_following)
          get :follow, { id: user_following.id }
          expect(response).to render_template(nil)
        end
      end
    end

    context "user not logged" do
      it "redirects to user loggin page" do
        get :follow, { id: user.id }

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe "GET following" do
    context "user logged" do
      before do
        sign_in user
      end

      it "update the wall header" do
          get :following, { id: user_following.id }
          expect(response).to render_template("following")
      end
    end

    context "user not logged" do
      it "redirects to user loggin page" do
        get :following, { id: user_following.id }

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe "GET followed" do
    context "user logged" do
      before do
        sign_in user
      end

      it "update the wall header" do
          get :followers, { id: user_not_following.id }
          expect(response).to render_template("followers")
      end
    end

    context "user not logged" do
      it "redirects to user loggin page" do
        get :followers, { id: user_not_following.id }

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
