require 'rails_helper'

RSpec.describe WelcomeController, :type => :controller do
  let(:user)    { FactoryGirl.create(:user) }

  describe "GET index" do
    context "user logged" do
      before { sign_in user }

      it "rendirects to the user page" do
        get :index

        expect(response).to render_template("index")
      end
    end

    context "user not logged" do
      it "redirects to user loggin page" do
        get :index

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
