require 'rails_helper'

RSpec.describe Post, :type => :model do
  subject      { FactoryGirl.create(:post) }

  describe "#content" do
    let(:field)       { :content }
    let(:max_length)  { 160 }

    it_behaves_like "field can't be nil"
    it_behaves_like "field can't be blank"
    it_behaves_like "field can't overcome max_length"
 end

  describe "#user_id" do
    let(:field)  { :user_id }

    it_behaves_like "field can't be nil"
    it_behaves_like "field can't be blank"
  end
end
