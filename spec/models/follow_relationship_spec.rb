require 'rails_helper'

RSpec.describe FollowRelationship, :type => :model do
  subject      { FactoryGirl.create(:follow_relationship) }

  describe "#follower_id" do
    let(:field)       { :follower_id }
    it_behaves_like "field can't be nil"
    it_behaves_like "field can't be blank"
  end

  describe "#followed_id" do
    let(:field)       { :followed_id }
    it_behaves_like "field can't be nil"
    it_behaves_like "field can't be blank"
  end
end
