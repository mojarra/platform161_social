require 'rails_helper'

RSpec.describe User, :type => :model do
  subject                    { FactoryGirl.create(:user) }
  let(:user_following)       { FactoryGirl.create(:user) }
  let(:user_not_following)   { FactoryGirl.create(:user) }
  let!(:follow_relationship) { FactoryGirl.create(:follow_relationship,
                                                   follower: subject,
                                                   followed: user_following) }

  describe "#username" do
    let(:field)  { :username }

    it_behaves_like "field can't be nil"
    it_behaves_like "field can't be blank"
    it_behaves_like "field should be uniq"
    it_behaves_like "field should contains only '.', numbers and letters"
 end

  describe "#email" do
    let(:field)  { :email }

    it_behaves_like "field can't be nil"
    it_behaves_like "field can't be blank"
    it_behaves_like "field should be uniq"
  end

  describe "#login" do
    context "@login assigned" do
      before { subject.login = "login" }

      it { expect(subject.login).to eq("login")  }
    end

    context "@login not assigned" do
      it { expect(subject.login).to eq(subject.username) }
    end
  end

  describe "self#find_for_database_authentication" do
    context "by username" do
      it { expect(find_by_login(subject.username)).to eq(subject) }
    end

    context "by username" do
      it { expect(find_by_login(subject.email)).to eq(subject) }
    end

    context "non existing login" do
      it { expect(find_by_login("non_existing_login")).to eq(nil) }
    end
  end

  describe "#follow(user)" do
    it { expect(subject.following).to     include(user_following) }
    it { expect(subject.following).to_not include(user_not_following) }
  end

  describe "unfollow(user)" do
    it {
      expect(subject.following).to     include(user_following)
      subject.unfollow(user_following)
      expect(subject.following).to_not include(user_following)
    }
  end

  describe "#following?" do
    it { expect(subject.following?(user_following)).to be_truthy }
    it { expect(subject.following?(user_not_following)).to be_falsey }
  end

  describe "#feed" do
    let!(:user_post)               { FactoryGirl.create(:post, user: subject) }
    let!(:following_user_post)     { FactoryGirl.create(:post, user: user_following) }
    let!(:not_following_user_post) { FactoryGirl.create(:post, user: user_not_following) }

    it { expect(subject.feed).to     include(user_post) }
    it { expect(subject.feed).to     include(following_user_post) }
    it { expect(subject.feed).to_not include(not_following_user_post) }
  end

  def find_by_login(login)
    described_class.find_for_database_authentication({ login: login })
  end
end
