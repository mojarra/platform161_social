# README #

### What is this repository for? ###

* Quick summary

This platform161_social project. Platform161 has decided to change its business model and is going to create something new, a social network. The app include thesee features:

- A user can register using his email, a selected password and a user name. The user name has to be unique.

- One user can publish messages no longer than 160 characters.

- One user can follow what other users publish. When the user accesses the application, he will see the messages published by the users he follow.

- Everyone can see the users followed by any user.

- Everyone can see the users that follow one user.

But other developers should be able to build their own applications using these public information. So, provide an API access to allow:

- See one user messages.

- See the users followed by one user.

- See the users that follow one user.

* Version
V1.0

### How do I get set up? ###

* Ruby version
  Ruby 2.2.1

* Summary of set up
  - bundle install
  - bower install
  - cp config/database.yml.example config/database.yml
  - bundle exec rake db:create
  - bundle exec rake db:migrate
  - bundle exec rake data:generate_api_key
  - bundle exec rake data:generate_random_test_data
  - bundle exec rails s


  After that to access to the server go to your browser and got to localhost:3000 to normal web access with the instructions generated in the rake process generate_random_test_data or if you prefer to accesses to the API with the access token generated in the generate_api_key rake proces you can query with curl like in the next example:

  curl http://localhost:3000/api/users/3 -H 'Authorization: Token token="<acces_token_generated>"'

* configuration

* Dependencies
* Database configuration
  The project is right now on Sqllite if you want to add a different DB you have to add your DB connector to the Gemfile change the configuration in config/database.yml and recreate the db
* How to run tests
  rspe /spec
* Deployment instructions
  not defined

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
  mojarra(@mojarraGandhi)
* Other community or team contact
