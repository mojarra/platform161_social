require 'api_constraints'

Rails.application.routes.draw do
  devise_for :users

  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :users, only: %w[ show ] do
        member do
          get 'following'
          get 'followers'
        end
      end
    end
  end

  resources :users, only: %w[ show ] do
    member do
      get 'follow'
      get 'following'
      get 'followers'
    end
  end

  resources :posts, only: %w[ create ]

  root 'welcome#index'
end
